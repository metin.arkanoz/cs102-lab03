
public class Car {
		
	// Just for fun2.
	private int odometer;
	private String brand;
	private String color;
	private int gear = 0;
	private int travelledHour = 0;
	
	public Car() {
		odometer = 0;
		brand = "Renault";
		color = "Red";
		
	}
	
	
	public Car(int odometer,String brand,String color,int gear) {		// accepts parameters.
		// TODO Auto-generated constructor stub
		this.brand = brand;
		this.odometer = odometer;
		this.color = color;
		this.gear = gear;
		
	}
	
	public void incrementGear() {
		if(this.gear == 5)System.out.println("Gear should be between 0 and 5 !");
		else this.gear+=1;
	}
	
	public void decrementGear() {
		if(this.gear == 0)System.out.println("Gear should be between 0 and 5 !");
		else this.gear-=1;
	}
	
	public void drive(int hour,int velocityPerHour) {
		
		this.travelledHour+= hour;
		this.odometer+=hour*velocityPerHour;
	}
	
	public void getOdometer() {System.out.println("Odometer: "+this.odometer);}
	
	public void setOdometer(int km) {this.odometer = km;}
	
	public void getBrand() {System.out.println("Brand: "+this.brand);}
	
	public void setBrand(String brand) {this.brand = brand;}
	
	public void getColor() {System.out.println("Color: "+this.color);}
	
	public void setColor(String color) {this.brand = color;}
	
	public void getGear() {System.out.println("Gear: "+this.gear);}
	
	public void setGear(int gear) {
		if(gear<0 || gear>5)System.out.println("Gear should be between 0 and 5 !");
		else this.gear = gear;}
	
	public void display() {
		System.out.println("Odomoter: "+this.odometer);
		System.out.println("Brand :"+ this.brand);
		System.out.println("Color :"+this.color);
		System.out.println("Gear  :"+this.gear);
		if(this.travelledHour == 0) {
		System.out.println("Average Speed: HAS NOT BEEN DRIVEN YET!");}
		else System.out.println("Average Speed: "+ this.odometer/ (double)this.travelledHour);
	}
	
	
}
